const getSum = (str1, str2) => {
  if(typeof(str1)!='string' || typeof(str2)!='string' ){
    return false;
  }
  if (str1 == ''){
    return str2;
  }
  if (str2 == ''){
    return str1;
  }
  for(let i = 0;i<str1.length;i++){
    if(isNaN(str1[i])){
      return false;
    }
  }
  for(let i = 0;i<str2.length;i++){
    if(isNaN(str2[i])){
      return false;
    }
  }
  var str3 = '';
  for(let i = 0;i<str1.length;i++){
    str3+=(Number(str1[i])+Number(str2[i])).toString();
  }
  return str3;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let com = 0;
  for(let post of listOfPosts){
    if(post.author == authorName){
      posts++;
    }
    if(post.comments!= undefined){
      for(let comment of post.comments){
        if(comment.author == authorName){
          com++;
        }
      }
    }
  }
  return 'Post:'+posts+ ',comments:'+com;
};

const tickets=(people)=> {
  let money = 0;
  let price = 25;
  for(let p of people){
    if(p>(money + price)){
      return 'NO';
    }else{
      money = money + price;
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
